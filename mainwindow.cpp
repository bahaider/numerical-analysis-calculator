#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
#include <QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::input_output()
{
    QString exp = ui -> lineEdit -> text();
    Output *new_dialog = new Output();
    new_dialog -> setWindowTitle("Input --> Output");
    new_dialog -> alterlineEdit(exp);
    new_dialog -> show();
}

void MainWindow::lagrange()
{
    QString exp = ui -> lineEdit -> text();
    LaGrange *new_dialog = new LaGrange();
    new_dialog -> setWindowTitle("LaGrange Interpolating Polynomial");
    new_dialog -> alterlineEdit(exp);
    new_dialog -> show();
}

void MainWindow::derivative()
{
    QString exp = ui -> lineEdit -> text();
    Derivative *new_dialog = new Derivative();
    new_dialog -> setWindowTitle("First Derivative");
    new_dialog -> alterlineEdit(exp);
    new_dialog -> show();
}

void MainWindow::second_derivative()
{
    QString exp = ui -> lineEdit -> text();
    Second_Derivative *new_dialog = new Second_Derivative();
    new_dialog -> setWindowTitle("Second Derivative");
    new_dialog -> alterlineEdit(exp);
    new_dialog -> show();
}

void MainWindow::integral()
{
    QString exp = ui -> lineEdit -> text();
    Integral *new_dialog = new Integral();
    new_dialog -> setWindowTitle("Definite Integral");
    new_dialog -> alterlineEdit(exp);
    new_dialog -> show();
}

void MainWindow::zero()
{
    QString exp = ui -> lineEdit -> text();
    Zero *new_dialog = new Zero();
    new_dialog -> setWindowTitle("Equation Solver");
    new_dialog -> alterlineEdit(exp);
    new_dialog -> show();
}

void MainWindow::rungekutta()
{
    QString exp = ui -> lineEdit_2 -> text();
    RungeKutta *new_dialog = new RungeKutta();
    new_dialog -> setWindowTitle("Initial Value Problem Solver");
    new_dialog -> alterlineEdit(exp);
    new_dialog -> show();
}
void MainWindow::on_actionAbout_triggered()
{
    info = new About(this);
    info -> setWindowTitle("About");
    info -> show();
}
