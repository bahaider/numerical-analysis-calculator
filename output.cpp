#include "output.h"
#include "ui_output.h"
#include "mainwindow.h"

Output::Output(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Output)
{
    ui->setupUi(this);
}

Output::~Output()
{
    delete ui;
}

void Output::alterlineEdit(QString new_string)
{
    ui -> lineEdit -> setText(new_string);
}

void Output::func_output()
{
    QString expression = ui->lineEdit->text();
    std::string exp_copy = expression.toLocal8Bit().constData();
    Function a(exp_copy);
    double input = ui -> lineEdit_2 ->text().toDouble();
    double answer = a.output(input);
    QString str_answer = QString::number(answer);
    ui -> lineEdit_3 -> setText(str_answer);
}
