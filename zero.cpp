#include "zero.h"
#include "ui_zero.h"
#include "mainwindow.h"

Zero::Zero(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Zero)
{
    ui->setupUi(this);
}

Zero::~Zero()
{
    delete ui;
}

void Zero::alterlineEdit(QString new_string)
{
    ui -> lineEdit -> setText(new_string);
}

void Zero::func_zero()
{
    QString expression = ui->lineEdit->text();
    std::string exp_copy = expression.toLocal8Bit().constData();
    Function a(exp_copy);
    double initial = ui -> lineEdit_2 ->text().toDouble();
    double TOL = ui -> lineEdit_3 ->text().toDouble();
    double max = ui -> lineEdit_4 ->text().toDouble();
    double answer = Newtons(a, initial, TOL, max);
    QString str_answer = QString::number(answer);
    ui -> lineEdit_5 -> setText(str_answer);
}
