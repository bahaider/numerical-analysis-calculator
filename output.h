#ifndef OUTPUT_H
#define OUTPUT_H

#include <QDialog>
#include <QString>
#include <QLineEdit>

namespace Ui {
class Output;
}

class Output : public QDialog
{
    Q_OBJECT
    
public:
    explicit Output(QWidget *parent = 0);
    ~Output();
    void alterlineEdit(QString new_string);

private slots:
    void func_output();

private:
    Ui::Output *ui;
};

#endif // OUTPUT_H
