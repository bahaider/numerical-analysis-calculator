#ifndef ZERO_H
#define ZERO_H

#include <QDialog>

namespace Ui {
class Zero;
}

class Zero : public QDialog
{
    Q_OBJECT
    
public:
    explicit Zero(QWidget *parent = 0);
    ~Zero();
    void alterlineEdit(QString new_string);

private slots:
    void func_zero();
    
private:
    Ui::Zero *ui;
};

#endif // ZERO_H
