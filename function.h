/**
@mainpage Numerical Analysis Calculator
@date May 24, 2013
@version 1.1
@brief A set of numerical methods used to perform certain mathematical operations.

@section Introduction

* Numerical Analysis Calculator is a library of iterative algorithms from Numerical Analysis.
* The optimized C++ implementations of the following numerical methods are available:
*
* Solutions of Equations in One Variable
*
*   - Newton's Method
*   - Fixed Point Method
*   - Secant Method
*
* Interpolation and Polynomial Approximation
*
*   - nth Lagrange Interpolating Polynomial
*
* Numerical Differentiation & Integration
*
*   - Three-Point Midpoint
*   - Five-Point Midpoint
*   - Composite Simpson's Rule for Numerical Integration
*
* Initial Value Problems for Solving Ordinary Differential Equations**
*
*   - Runge-Kutta Method
*
* Large Systems of Linear Equations
*
*   - Gauss-Seidel Iterative Methods
*
* A simple GUI was created in QtCreator for ease of use.
*
* ![main_menu.png](https://bitbucket.org/repo/jnLydX/images/2432404529-main_menu.png)[1]
*
*/

#ifndef FUNCTION_H
#define FUNCTION_H

#include <string>
#include<vector>

using namespace std;

/**
@class Function function.h "function.h"
@brief Single variable function class.

@details
This class is designed to be the base class of function objects. \e Function objects store
a single string representing a math expression \b f(x). Various member functions perform
numerical methods on this object (Refer to a specific member function to see how it works.)

EXAMPLE:\n
Function cubic("x^3");\n
cout << cubic.output(2);//outputs 8\n
*/
class Function
{
public:
	Function();

	/**
	Function creates a \e Function class object.
	@param equation is the expression \b f(x)
	*/
	Function(string equation);

	/**
	@return the expression \b f(x) as a string
	*/
	string get_function() const;

    /**
    @brief Changes the private string in a \e Function class object
    @param new_func is the new expression \b f(x)
    */
    void change_function(string new_func);

	/**
	Output evaluates a \e Function object for a single input \e x.
	@param x is the independent variable
	*/
	double output(double x);

	/**
	Lagrange evaluates the Lagrange Interpolating polynomial of a \e Function object for a single input.
	@param nodes is the vector holding all the x-coordinates of every point used in the interpolation.
	@param x is the value for at which the interpolating polynomial is evaluated.
	*/
	double Lagrange(vector <double> nodes, double x);

	/**
	Approximates the derivative of a \e <Function> object using a 3-Point Formula.
	@param increment is the amount from which we stray from the desired x-coordinate.
		(Just know that the approximation is more accurate for smaller values of \e<increment>
		(ex. 0.01 or 0.001)).
	@param input is the value at which the derivative is evaluated.
	*/
	double der_3pt(double increment, double input);

	/**
	Approximates the derivative of a \e <Function> object using a 5-Point Formula.
	@param increment is the amount from which we stray from the desired x-coordinate.
		(Just know that the approximation is more accurate for smaller values of \e<increment>
		(ex. 0.01 or 0.001)).
	@param input is the value at which the derivative is evaluated.
	*/
	double der_5pt(double increment, double input);

	/**
	Approximates the second derivative of a \e <Function> object.
	@param increment is the amount from which we stray from the desired x-coordinate.
		(Just know that the approximation is more accurate for smaller values of \e<increment>
		(ex. 0.01 or 0.001)).
	@param input is the value at which the second derivative is evaluated.
	*/
	double second_der(double increment, double input);

	/**
	Approximates the definite integral of a \e <Function> object.
	@param interval_beg is the lower bound of the definite integral.
	@param interval_end is the upper bound of the definite integral.
	@int num_nodes is the number of nodes between the interval. (The approximation
	is more accurate for larger values of \e <num_nodes>).
	*/
	double comp_simpsons(double interval_beg, double interval_end, int num_nodes);
private:
	string expression;
};


/**
@class IVP function.h "function.h"
@brief Initial Value Problem class.

@details
This class is a derived class of the \e Function class. The data field \e expression
inherited from the \e Function class stores an expression of the form \b y'

EXAMPLE:\n
For the IVP \b y' = y-(x^2)+1, 0 <= x <= 2, y(0) = 0.5 we would write:\n
IVP problem("(y-(x^2))+1", 0, 2, 0.5);\n
cout << problem.Runge_Kutta(10); //outputs an approximated solution
*/
class IVP : public Function
{
public:

	/**
	Creates an \e IVP class object.
	@param equation is the expression \b y'
	@param a is the lower bound of the problem
	@param b is the upper bound of the problem
	@param ic is the initial condition, y(a) = ic
	*/
	IVP(string equation, double a, double b, double ic);

	/**
	Solves for y(b) (Evaluates the solution of the IVP at the end of the interval)
	@param N is the number of divisions of the interval [a, b]. (So we approximate
	values of \b y for (N+1) equally spaced nodes in [a,b].)
	*/
	double Runge_Kutta(int N);
private:
	double interval_beg;
	double interval_end;
	double init_cond;
};

/**
@class Matrix function.h "function.h"
@brief Matrix class.

@details
This class is designed to create matrices to solve linear systems of equations.
A \e Matrix object is created by specifying its dimensions and it is filled
through a member function. A linear system \b Ax = \b b is created and solved through the
\e Gauss_Seidel member function.

EXAMPLE:\n
For the 3x3 matrix [(3, -1, 1), (3, 6, 2), (3, 3, 7)] we would write:\n
Matrix A(3, 3);\n
A.fill_matrix(); //You will be asked to fill the matrix BY ROW (HORIZONTALLY)\n
A.Gauss_Seidel(10);
*/
class Matrix
{
public:
	Matrix();

	/**
	Creates a \e Matrix class object.
	@param n is the number of rows.
	@param m is the number of columns.
	*/
	Matrix(int n, int m);

	/**
	Fills a \e Matrix object BY ROW (HORIZONTALLY)
	*/
	void fill_matrix();
	
	/**
	Outputs a matrix to console.
	*/
	void output_matrix();

	/**
	Changes the value of the index i,j for a \eMatrix object.
	@param i is to the row of the index
	@param j is the column of the index
	*/
	void alter_matrix(int i, int j, double value);

	/**
	Returns the indexes of a \eMatrix object in a vector of vectors.
	Each inner vector holds values of a row.
	*/
    vector <vector <double> > get_matrix();

	/**
	Solves a linear system of equations \b Ax = \b b. Console will ask
	that you input the vector \b b and the initial approximation vector \x
	(which is usually the zero vector). The solution is outputted to the console.
	*/
	void Gauss_Seidel(int max_iter);
private:
	int rows;
	int columns;
    vector <vector <double> > matrix;
};

/**
@brief Approximates the zero of a \eFunction class object using Newton's Method.
**Only for single variable functions.
@param a is the \eFunction object whose zero we're approximating.
@param init_approx is the initial approximation (some value of x near the zero).
@param TOL is the tolerance (the degree of accuracy ex. 1e-8).
@param max_iter is the maximum number of iterations.
@return Returns an approximation of the zero of the function.
*/
double Newtons(Function a, double init_approx, double TOL, int max_iter);

/**
@brief Approximates the fixed point of a \eFunction object for \b x = \bf(x).
**Only for single variable functions.
@param a is the \eFunction object whose fixed point we're approximating.
@param init_approx is the initial approximation (some value of x near the fixed point).
@param TOL is the tolerance (the degree of accuracy ex. 1e-8).
@param max_iter is the maximum number of iterations.
@return Returns an approximation of the fixed point of the function.
*/
double fixed_point(Function a, double init_approx, double TOL, int max_iter);

/**
@brief Approximates the zero of a \eFunction class object using the Secant Method.
**Only for single variable functions.
@param a is the \eFunction object whose zero we're approximating.
@param init_approx0 is our first initial approximation (some value of x near the zero).
@param init_approx1 is our second initial approximation.
@param TOL is the tolerance (the degree of accuracy ex. 1e-8).
@param max_iter is the maximum number of iterations.
@return Returns an approximation of the zero of the function.
*/
double secant_method(Function a, double init_approx0, double init_approx1, double TOL, int max_iter);

/**
@brief Refer to \eparse_expression
*/
double parse_factor(string input, int& len, double x, double y);

/**
@brief Refer to \eparse_expression
*/
double parse_term(string input, int& len, double x, double y);

/**
@brief Evaluates a string representing a mathematical expression for 
2 independent variables by parsing.\n

Parser supports negative numbers and the following functions:\n
sin, cos, tan, exp (e^x), ln (natural log), 
log (log base 10), abs (absolute value).\n


****IMPORTANT****\n
1) When writing a string for parsing, if you have an expression of the form x^y it must be enclosed in parentheses [ex. "(x^(1+2))+1" ]\n
2) When using parse_function, add a blank space to the end of the expression [ex. parse_function(expression + " "....)]\n
3) Do NOT use spaces when writing function class object strings\n
4) For every consecutive operation, use parenthesis to enclose the previous operation [ex. write "((1-2)-3)-4", not "1-2-3-4"
(sorry =(, I know that's annoying)].\n

@param input is the math expression as a string (add empty space at the end)
@param len is the placeholder in the expression (always set to 0)
@param x is the input for the independent variable x
@param y is the input for the independent variable y
*/
double parse_expression(string input, int& len, double x, double y);

#endif
