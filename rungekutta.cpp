#include "rungekutta.h"
#include "ui_rungekutta.h"
#include "mainwindow.h"

RungeKutta::RungeKutta(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RungeKutta)
{
    ui->setupUi(this);
}

RungeKutta::~RungeKutta()
{
    delete ui;
}

void RungeKutta::alterlineEdit(QString new_string)
{
    ui -> lineEdit_4 -> setText(new_string);
}

void RungeKutta::RKutta()
{
    QString expression = ui->lineEdit_4->text();
    std::string exp_copy = expression.toLocal8Bit().constData();
    double lower_bound = ui -> lineEdit_5 ->text().toDouble();
    double upper_bound = ui -> lineEdit_6 ->text().toDouble();
    double initial_condition = ui -> lineEdit_8 ->text().toDouble();
    IVP a(exp_copy, lower_bound, upper_bound, initial_condition);
    int divisions = ui -> lineEdit_9 ->text().toInt();
    double answer = a.Runge_Kutta(divisions);
    QString str_answer = QString::number(answer);
    ui -> lineEdit_11 -> setText(str_answer);
}
