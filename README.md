# **Numerical Analysis Calculator** #

## **About** ##
Numerical Analysis Calculator was created as a project for my Program in Computing 10C (Advanced Programming with C++) class at UCLA. The assignment was to develop software that incorporated knowledge of a mathematical subject of our choosing. Since I was simultaneously taking a course in Numerical Analysis , I opted to create a calculator as an aid for my class assignments.

The project is a library of the iterative algorithms I studied in my Numerical Analysis course. A simple GUI was created in QtCreator for ease of use.
Several numerical methods are available for the following topics to anyone interested in their optimized C++ implementation:

**Solutions of Equations in One Variable**

* Newton's Method
* Fixed Point Method
* Secant Method

**Interpolation and Polynomial Approximation**

* nth Lagrange Interpolating Polynomial

**Numerical Differentiation & Integration**

* Three-Point Midpoint
* Five-Point Midpoint
* Composite Simpson's Rule for Numerical Integration

**Initial Value Problems for Solving Ordinary Differential Equations**

* Runge-Kutta Method

**Large Systems of Linear Equations**

* Gauss-Seidel Iterative Methods

## **How to Use** ##
In Qt, open NumericalAnalysisCalculator.pro and Qt will configure and build the project. Be sure that your Path environment variable has the directory of the Visual C++ compiler (cl.exe). For windows, it is usually under the following directory depending on your version of Visual Studio:

```
#!c++

C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin
```

## **Interface** ##
Running the program will display a main menu [1] where you can input a function or an initial value problem (refer to the *About* tab for syntax and general use). From here, you can select which iterative method you would like to perform and the corresponding menu will pop up.

![main_menu.png](https://bitbucket.org/repo/jnLydX/images/2432404529-main_menu.png)[1]

Refer to function.h for the documentation of each of the available methods.